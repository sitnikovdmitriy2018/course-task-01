const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = (process.env.PORT == undefined) ? 56201 : process.env.PORT;

app.use(bodyParser.raw({ inflate: true, limit: '100kb', type: 'text/plain' }));


app.post('/square', (req, res) => {
  console.log(req.body);
  const number = parseInt(req.body);

  if (isNaN(number)) {
    res.status(500);
    res.send("Is not a number");
  } else {
    res.setHeader('content-type', 'application/json');
    res.send({
      number: number,
      square: number*number
    });
  }
})



app.post('/reverse', (req, res) => {
  const string = "" + req.body;
  console.log(req.body);
  const reversedString = string.split("").reverse().join("");

  res.setHeader('content-type', 'text/plain');
  res.send(reversedString);
})



app.get('/date/:year/:month/:day', (req, res) => {
  const d = new Date(parseInt(req.params.year), parseInt(req.params.month) - 1, parseInt(req.params.day));
  const n = Date.now();
  const weekDay = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][d.getDay()];
  const isLeapYear = (d.getYear() % 4) == 0;
  const difference =  (d>n) ? (Math.ceil((d-n)/(1000 * 60 * 60 * 24))) : (Math.ceil((n-d)/(1000 * 60 * 60 * 24)) - 1);

  res.setHeader('content-type', 'application/json');
  res.send({  
    weekDay: weekDay,
    isLeapYear: isLeapYear,
    difference: difference
  });
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
